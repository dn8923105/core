# HOOK_WARENKORB_PAGE_KUPONANNEHMEN_PLAUSI (55)

## Triggerpunkt

Plausibilitätsprüfung für die Annahme eines Kupons, im Warenkorb

## Parameter

* `array` **&error** (ab 4.05) - aufgetretene Fehler beim Annehmen des Kupons
* `int` **&nReturnValue** - Resultat der Prüfung