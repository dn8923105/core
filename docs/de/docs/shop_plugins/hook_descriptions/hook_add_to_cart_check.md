# HOOK_ADD_TO_CART_CHECK (261)

## Triggerpunkt

Am Ende der Prüfung "zum Warenkorb hinzufügen"

## Parameter

* `JTL\Catalog\Product\Artikel` **product** - Artikelobjekt
* `int` **quantity** - Anzahl
* `array` **attributes** - Artikelattribute
* `array` **&redirectParam** - Redirect-Parameter
