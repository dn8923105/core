# HOOK_KUNDE_CLASS_HOLLOGINKUNDE (145)

## Triggerpunkt

Nach dem Holen eines Kundenlogins, vor der Entschlüsselung

## Parameter

* `JTL\Customer\Kunde` **&oKunde** - Kundenobjekt
* `stdClass` **oUser** - Benutzerobjekt (noch unidentifiziert)
* `string` **cBenutzername** - Benutzername
* `string` **oKucPasswortnde** - Passwort