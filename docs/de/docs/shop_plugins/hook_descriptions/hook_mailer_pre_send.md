# HOOK_MAILER_PRE_SEND (291)

## Triggerpunkt

Direkt vor dem Senden einer Email via PHPMailer

## Parameter

* `\JTL\Mail\Mailer` **mailer** - Mailer-Objekt
* `\JTL\Mail\Mail\MailInterface` **mail** - Mail-Objekt
* `\PHPMailer\PHPMailer\PHPMailer` **phpmailer** - PHPMailer-Instanz