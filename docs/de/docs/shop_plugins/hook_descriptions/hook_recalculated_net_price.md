# HOOK_RECALCULATED_NET_PRICE (321)

## Triggerpunkt

Nach der Neuberechnung des Nettopreises

## Parameter

* `float|string` **netPrice** - Nettopreis
* `float|string` **defaultTax** - Standard Steuersatz des Artikels
* `float|string` **conversionTax** - Steuersatz des Lieferlandes
* `float|string` **&newNetPrice** - neuer berechneter Nettopreis